# Music Orchestrator Server
This is the server/web frontend of the music orchestrator.

# Software Setup
To run the Music Orchestrator server, you'll need:
* Python 3.5 or later (Install from `python.org`)
* If you'd like your orchestrator to be accessible via the internet, a web server that can serve WSGI applications (Apache2 w/ mod_wsgi for example). Otherwise, you can use the built-in flask server.
* The Flask and Flask-SocketIO packages (`pip3 install flask flask-socketio`)

## Other things to set up
The v2.7 mood setup used in the live stream is included here.

If you'd like to customize your mood preferences, go to this spreadsheet:

https://docs.google.com/spreadsheets/d/1X1jTqVIYP3lQV1QNF04Mokux6oRLWRP9D7QqiGOGcSo/edit?usp=sharing

Make a copy of this spreadsheet using the v2.7 tab, change it as you'd like, then export that tab to mood.tsv. All rows must add up to 1 for the low/medium/high speed preference chance. Place that in the directory that your server is running in.

At this time, the music orchestrator is single-genre. In the future, I'll likely add some improvements so the orchestrator can handle multiple genres.

# Server Setup
The client docs mention two methods of setting up a server.

## Method 2 - Local hosting with Flask's built-in web server
With Method 2, you can just modify the mood settings and not have people access the orchestrator page. This will use Flask's built in server on port 5000, accessible via `http://localhost:5000`.

If you'd like the rest of your local network to see the orchestrator page using flask's built in server, add the argument `host=0.0.0.0` to `socketio.run()` on the last line of code.

## Method 3 - Hosting on a web server with a WSGI module
With Method 3, You can set up the server by using a web server (Apache2/NGINX) with a WSGI module, if you have a domain that you can use for the orchestrator. This will also allow you to point visitors to a page so that they can see what's playing on the orchestrator. I won't go into detail about WSGI setup here, but a simple script that adds the orchestrator to your path works fine. One is included for reference.

Get the configuration set up - but don't go live just yet. There's some additional configuration you'll need to do.

# Configuration Setup
Two things to configure here. They're configured in the Python script, rather than an external file (due to the small amount of stuff to configure).

`key` is used to authenticate the client to post song data to your server. Make this a random password or what have you, this is the key the client uses to post up song data to the server. Make sure you save it for the client. 

`cors_origin` is used for the Socket.IO system, this is basically where you want to allow Socket.IO connections from. The hostname of the server hosting things works fine. If you don't care about CORS or don't know what CORS is, set this to `*`.

# Finalizing setup

## Method 2
Run `python3 server.py` in a command prompt (`python server.py` on Windows). The orchestrator server should be running! You can check by visiting `http://localhost:5000` in your browser.

## Method 3
Get the configuration up and running on your server - and get things started. The orchestrator should be running! Check by visiting wherever your orchestrator is being hosted.


You're now completed with setup! You can now configure `page.html` under templates to your liking.

Once you get the client setup, if data isn't refreshing on its own after a song ends, you likely had the Flask-SocketIO library already installed and it needs to be upgraded.

You can do that by running `pip3 install --upgrade flask-socketio`.