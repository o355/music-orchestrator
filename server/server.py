# Music Orchestrator Server
# (c) 2021  Owen McGinley
# Licensed under the MIT License

from flask import Flask, request, render_template, jsonify
from flask_socketio import SocketIO, join_room, leave_room, emit, rooms
import json
import random
from datetime import datetime
import csv
import time

cors_origins = "*"
post_key = "pleasechangethis"

with open("mood.tsv") as f:
    mooddict = list(csv.reader(f, delimiter="\t", quotechar='"'))

def corsify(response):
    response = jsonify(response)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins=cors_origins, async_handlers=True)


@socketio.on("join")
def on_join(message):
    emit("joined", "Joined the websocket!")


@app.route("/api/v1/getMoodSelection", methods=['GET'])
def gMS():
    curdate = datetime.now()
    minute = curdate.minute // 30
    hour = curdate.hour
    mood_pick_str = ""
    speed_pick_str = ""
    base_index = 0
    cat_pick = random.uniform(0, 1)
    mood_pick = random.uniform(0, 1)
    if cat_pick <= float(mooddict[(hour * 2) + minute + 1][1]):
        mood_pick_str = "jazz"
        base_index = 2

    if mood_pick <= float(mooddict[(hour * 2) + minute + 1][base_index]):
        speed_pick_str = "low"
    elif mood_pick <= float(mooddict[(hour * 2) + minute + 1][base_index]) + float(mooddict[(hour * 2) + minute + 1][base_index + 1]):
        speed_pick_str = "med"
    else:
        speed_pick_str = "high"

    response = {"apiversion": "1.0.0", "cat_pick": cat_pick, "mood_pick": mood_pick, "rawpick": mooddict[(hour * 2) + minute + 1][1], "mood": mood_pick_str, "speed": speed_pick_str}
    return corsify(response)


@app.route("/api/v1/postSongData", methods=['POST'])
def pSD():
    key = request.values.get("key")
    if key != post_key:
        response = {"apiversion": "1.0.0", "message": "Invalid key"}
        return corsify(response)

    song = request.values.get("song")
    artist = request.values.get("artist")
    time = request.values.get("time")
    duration = request.values.get("duration")
    moodpicked = request.values.get("mood")
    speedpicked = request.values.get("speed")
    last25 = json.loads(request.values.get("last25"))

    jsondata = {"song": song, "time": time, "duration": duration, "artist": artist, "moodpicked": moodpicked, "speedpicked": speedpicked, "last25": last25}
    with open("songdata.json", "w") as f:
        json.dump(jsondata, f)

    socketio.emit("songupdate", "")

    response = {"apiversion": "1.0.0", "message": "Success"}
    return corsify(response)


@app.route("/api/v1/getInfo", methods=['GET'])
def gI():
    with open("songdata.json") as f:
        songinfo = json.load(f)

    curdate = datetime.now()
    minute = curdate.minute // 30
    hour = curdate.hour
    row = (hour * 2) + minute + 1


    response = {"apiversion": "1.0.0", "time": songinfo['time'], "duration": songinfo['duration'], "song": songinfo['song'], "artist": songinfo['artist'], "moodpicked": songinfo['moodpicked'],
                "speedpicked": songinfo['speedpicked'], "last25songs": songinfo['last25'],
                "jazzpref": mooddict[row][1], "jazzlowpref": mooddict[row][2], "jazzmedpref": mooddict[row][3],
                "jazzhighpref": mooddict[row][4], "row": row}
    return corsify(response)


@app.route("/", methods=['GET'])
def default():
    return render_template("page.html")


if __name__ == "__main__":
    socketio.run(app)