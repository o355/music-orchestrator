# Music Orchestrator
Music Orchestrator project

# What is the music orchestrator?
The music orchestrator is a project built to dynamically control the jazz preferences of music on a live stream.

There's two parts - the client/categorization, and the server/web frontend. Both are written in Python. The server makes use of the Flask & Flask-SocketIO libraries.

Code for the client/categorization system is in `client`, while the server is in `server`. Those respective directories have READMEs with more information.

If you're more interested in getting the orchestrator running with a single-genre setup using my speed preferences - you just need to setup the client. If you want further mood customization, you'll need to run the server locally on your machine (using Flask's built-in server). Lastly, if you want to make the orchestrator web UI accessible to anyone, you'll need to run the server code on a proper web server that can handle hosting WSGI applications.

# License
The Music Orchestrator is licensed under the MIT License.