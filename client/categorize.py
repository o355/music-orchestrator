# Music Orchestrator Categorization Script
# (c) 2021  Owen McGinley
# Licensed under the MIT License

import glob
import json
import vlc
import os

prefdict = {
    "jazz": {
        "low": [],
        "med": [],
        "high": []
    }
}

musicfiles = glob.glob("*.mp3")
cwd = os.getcwd()
i = 1

try:
    with open("prefdict.json") as f:
        prefdict = json.load(f)
except:
    pass

for file in musicfiles:
    print("--------- (" + str(i) + "/" + str(len(musicfiles)) + ") ---------")
    jazz_lowlength = len(prefdict["jazz"]["low"])
    jazz_medlength = len(prefdict["jazz"]["med"])
    jazz_highlength = len(prefdict["jazz"]["high"])
    if (file in prefdict['jazz']['low']) or (file in prefdict['jazz']['med']) or (file in prefdict['jazz']['high']):
        i = i + 1
        continue
    jazz_totallength = jazz_lowlength + jazz_medlength + jazz_highlength
    print("Jazz: %s (%s L, %s M, %s H)" % (
        str(jazz_totallength), str(jazz_lowlength), str(jazz_medlength), str(jazz_highlength)
    ))
    print("Now playing: " + file)
    player = vlc.MediaPlayer("file:///" + cwd + "/" + file)
    player.play()

    speed = input("Is the song speed (L)ow, (M)edium, or (H)igh? ").lower()
    if speed == "l":
        speed = "low"
    elif speed == "m":
        speed = "med"
    elif speed == "h":
        speed = "high"
    type = "jazz"
    player.stop()
    prefdict[type][speed].append(file)
    with open("prefdict.json", "w") as outfile:
        json.dump(prefdict, outfile)
    i = i + 1
