# Music Orchestrator Client
# (c) 2021  Owen McGinley
# Licensed under the MIT License

import glob
import json
import vlc
import os
import requests
import random
import time
from configparser import ConfigParser

config = ConfigParser()
config.read("config.ini")

enable_posting = config.getboolean("SERVER", "enable_posting")
server_key = config.get("SERVER", "key")
server_host = config.get("SERVER", "host")

with open("prefdict.json") as f:
    prefdict = json.load(f)

last4songs = ["", "", "", ""]
last25songs = []

cwd = os.getcwd()

vlc_instance = vlc.Instance()
while True:
    response = requests.get(server_host + "/api/v1/getMoodSelection")
    response_dict = response.json()
    tries = 0
    while tries <= 100:
        songchoice = random.choice(prefdict[response_dict['mood']][response_dict['speed']])
        if songchoice not in last4songs:
            last4songs.pop(0)
            last4songs.append(songchoice)
            break
        else:
            tries = tries + 1

    if tries >= 100:
        continue

    media = vlc_instance.media_new("file:///" + cwd + "/" + songchoice)
    player = vlc_instance.media_player_new()
    player.set_media(media)
    player.play()
    timestarted = int(time.time())
    media.parse()
    song = media.get_meta(0)
    artist = media.get_meta(1)
    last25songs.insert(0, {"song": str(song), "artist": str(artist), "time": timestarted, "genre": response_dict['mood'], "speed": response_dict['speed']})
    if len(last25songs) > 25:
        last25songs.pop()

    last25_asstr = json.dumps(last25songs)
    time.sleep(0.01)
    duration = player.get_length() / 1000
    formdata = {
        "song": str(song),
        "artist": str(artist),
        "time": timestarted,
        "duration": duration,
        "mood": response_dict['mood'],
        "speed": response_dict['speed'],
        "key": server_key,
        "last25": last25_asstr,
    }
    if enable_posting:
        try:
            response = requests.post("https://orchestrator.owenthe.dev/api/v1/postSongData", data=formdata)
        except:
            print("Failed to post.")
    print("Now playing: " + str(song) + " by " + str(artist) + ". Set with genre " + response_dict['mood'] + " and speed " + response_dict['speed'])
    duration = player.get_length() / 1000
    time.sleep(duration)