# Music Orchestator Client
This is the client of the Music Orchestrator. It also includes a categorization script so you can easily categorize music.

# Software Setup
To run the categorization/client, you'll need:
* Python 3.5 or higher
* VLC Media Player (on Windows, you may need to install the 32-bit version of VLC for things to work. Otherwise, just have VLC installed to your system on OS X/Linux.)
* Install via pip: `pip3 install glob python-vlc requests configparser`

# Before you start
There's three ways you can set up the music orchestrator. Each involve different levels of difficulty and customization.
* **Method 1** - Using the preferences on my server. This is the easiest route, but there's no customization here. Your orchestrator will run at the preferences from my server. You also won't be able to have a web UI to show viewers what you're playing.
* **Method 2** - Using Flask's built-in server to host your own orchestrator server. This is a moderately hard route, which allows for customization of the preferences. However, viewers won't be able to use the web UI to see what's playing (only you'll have access on your machine).
* **Method 3** - Using a web server that can host WSGI applications. This is the hardest route. This allows for customization of preferences - and allows viewers to use the web UI to see what's playing.

If you're interested in method 2/3, set up the orchestrator server **FIRST** before continuing with the client setup. If you're interested in Method 1, continue reading these docs.

# Categorizing songs
The first thing you'll want to do is put all your stream music into the folder with the client. They must be in mp3 format - but you can adjust this in code. You should have at minimum 20-30 songs to avoid repetition.

Launch a command prompt in the client directory, then enter `python3 categorize.py`. The categorization script should fire up, and the first song should begin playing.

**On Windows, you may need to run `python categorize.py` instead.**

Go through all the songs, determining the speed (low, medium, high). Once complete, the script will save these to `prefdict.json`. If you add songs after the fact that have already been categorized, you'll only need to categorize those new songs.

# Configuring for client usage
**If you chose Method 1 earlier, skip this section. This only applies if you chose Method 2 or 3.**

First, set `enable_posting` to True. This will enable the script to post song data to your server (Method 2/3 ONLY - Method 1 should leave this to False).

The `key` should be what you set when doing the orchestrator server setup. Otherwise, song data won't post to your server.

Lastly, the `host` should be set to the server hosting the orchestrator server. This doesn't need a trailing slash. Leave this to `https://orchestrator.owenthe.dev` if you'd just like to use my server's preferences (Method 1). If you're doing local hosting of the server, set this to `http://localhost:5000` (Method 2). If you're using Method 3, put this to the host name of the server you're using.

# Client usage
Launch a command prompt in the client directory, then enter `python3 client.py`. It'll download the preferences for the time block, then begin playing music as categorized! If you are running your own server, then check the web frontend to see live details about the music playing.

**On Windows, you may need to run `python client.py` instead.**

The client will continue to run until stopped.

Enjoy! If you have any questions - open an issue report.